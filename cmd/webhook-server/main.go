package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"

	admission "k8s.io/api/admission/v1beta1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
)

const (
	//tlsDir      = `/run/secrets/tls`
	tlsDir      = `./`
	tlsCertFile = `tls.crt`
	tlsKeyFile  = `tls.key`

	jsonContentType = `application/json`
)

var (
	podResource = metav1.GroupVersionResource{Version: "v1", Resource: "pods"}

	universalDeserializer = serializer.NewCodecFactory(runtime.NewScheme()).UniversalDeserializer()
)

//admission_controll
// patchOperation is an operation of a JSON patch, see https://tools.ietf.org/html/rfc6902 .
type patchOperation struct {
	Op    string      `json:"op"`
	Path  string      `json:"path"`
	Value interface{} `json:"value,omitempty"`
}

// admitFunc is a callback for admission controller logic. Given an AdmissionRequest, it returns the sequence of patch
// operations to be applied in case of success, or the error that will be shown when the operation is rejected.
type admitFunc func(*admission.AdmissionRequest) ([]patchOperation, error)

// isKubeNamespace checks if the given namespace is a Kubernetes-owned namespace.
func isKubeNamespace(ns string) bool {
	return ns == metav1.NamespacePublic || ns == metav1.NamespaceSystem
}

// doServeAdmitFunc parses the HTTP request for an admission controller webhook, and -- in case of a well-formed
// request -- delegates the admission control logic to the given admitFunc. The response body is then returned as raw
// bytes.
func doServeAdmitFunc(w http.ResponseWriter, r *http.Request, admit admitFunc) ([]byte, error) {
	// Step 1: Request validation. Only handle POST requests with a body and json content type.

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return nil, fmt.Errorf("invalid method %s, only POST requests are allowed", r.Method)
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return nil, fmt.Errorf("could not read request body: %v", err)
	}

	if contentType := r.Header.Get("Content-Type"); contentType != jsonContentType {
		w.WriteHeader(http.StatusBadRequest)
		return nil, fmt.Errorf("unsupported content type %s, only %s is supported", contentType, jsonContentType)
	}

	// Step 2: Parse the AdmissionReview request.

	var admissionReviewReq admission.AdmissionReview

	if _, _, err := universalDeserializer.Decode(body, nil, &admissionReviewReq); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return nil, fmt.Errorf("could not deserialize request: %v", err)
	} else if admissionReviewReq.Request == nil {
		w.WriteHeader(http.StatusBadRequest)
		return nil, errors.New("malformed admission review: request is nil")
	}

	// Step 3: Construct the AdmissionReview response.

	admissionReviewResponse := admission.AdmissionReview{
		// Since the admission webhook now supports multiple API versions, we need
		// to explicitly include the API version in the response.
		// This API version needs to match the version from the request exactly, otherwise
		// the API server will be unable to process the response.
		// Note: a v1beta1 AdmissionReview is JSON-compatible with the v1 version, that's why
		// we do not need to differentiate during unmarshaling or in the actual logic.
		TypeMeta: metav1.TypeMeta{
			Kind:       "AdmissionReview",
			APIVersion: "admission.k8s.io/v1",
		},
		Response: &admission.AdmissionResponse{
			UID: admissionReviewReq.Request.UID,
		},
	}

	var patchOps []patchOperation
	// Apply the admit() function only for non-Kubernetes namespaces. For objects in Kubernetes namespaces, return
	// an empty set of patch operations.
	if !isKubeNamespace(admissionReviewReq.Request.Namespace) {
		patchOps, err = admit(admissionReviewReq.Request)
	}

	if err != nil {
		// If the handler returned an error, incorporate the error message into the response and deny the object
		// creation.
		admissionReviewResponse.Response.Allowed = false
		admissionReviewResponse.Response.Result = &metav1.Status{
			Message: err.Error(),
		}
	} else {
		// Otherwise, encode the patch operations to JSON and return a positive response.
		patchBytes, err := json.Marshal(patchOps)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return nil, fmt.Errorf("could not marshal JSON patch: %v", err)
		}
		admissionReviewResponse.Response.Allowed = true
		admissionReviewResponse.Response.Patch = patchBytes

		// Announce that we are returning a JSON patch (note: this is the only
		// patch type currently supported, but we have to explicitly announce
		// it nonetheless).
		admissionReviewResponse.Response.PatchType = new(admission.PatchType)
		*admissionReviewResponse.Response.PatchType = admission.PatchTypeJSONPatch
	}

	// Return the AdmissionReview with a response as JSON.
	bytes, err := json.Marshal(&admissionReviewResponse)
	if err != nil {
		return nil, fmt.Errorf("marshaling response: %v", err)
	}
	return bytes, nil
}

// serveAdmitFunc is a wrapper around doServeAdmitFunc that adds error handling and logging.
func serveAdmitFunc(w http.ResponseWriter, r *http.Request, admit admitFunc) {
	log.Print("Handling webhook request ...")

	var writeErr error
	if bytes, err := doServeAdmitFunc(w, r, admit); err != nil {
		log.Printf("Error handling webhook request: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, writeErr = w.Write([]byte(err.Error()))
	} else {
		log.Print("Webhook request handled successfully")
		_, writeErr = w.Write(bytes)
	}

	if writeErr != nil {
		log.Printf("Could not write response: %v", writeErr)
	}
}

// admitFuncHandler takes an admitFunc and wraps it into a http.Handler by means of calling serveAdmitFunc.
func admitFuncHandler(admit admitFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		serveAdmitFunc(w, r, admit)
	})
}

//master function
func applyApmDefaults(req *admission.AdmissionRequest) ([]patchOperation, error) {

	if req.Resource != podResource {
		//log.Printf("expect resource to be %s", podResource)
		return nil, nil
	}

	// Parse the Pod object.
	raw := req.Object.Raw
	pod := corev1.Pod{}
	if _, _, err := universalDeserializer.Decode(raw, nil, &pod); err != nil {
		return nil, fmt.Errorf("could not deserialize pod object: %v", err)
	}

	var volumeMount = &corev1.VolumeMount{
		Name:      "sw-agent",
		MountPath: "/skywalking/agent",
	}

	var volume = &corev1.Volume{
		Name: "sw-agent",
	}
	newVolumes := make([]corev1.Volume, 0, 0)
	for _, v := range pod.Spec.Volumes {
		newVolumes = append(newVolumes, v)
	}
	newVolumes = append(newVolumes, *volume)

	var iniContainer = &corev1.Container{
		Name:    "sw-agent-sidecar",
		Image:   "registry.cn-hangzhou.aliyuncs.com/shiqi/apm-sidecar:8.9.0",
		Command: []string{"sh"},
		Args: []string{
			"-c",
			"mkdir -p /skywalking/agent && cp -r /usr/skywalking/agent/* /skywalking/agent",
		},
		ImagePullPolicy: "IfNotPresent",
		VolumeMounts:    []corev1.VolumeMount{*volumeMount},
	}
	newInitContainer := make([]corev1.Container, 0, 0)
	for _, v := range pod.Spec.InitContainers {
		newInitContainer = append(newInitContainer, v)
	}
	newInitContainer = append(newInitContainer, *iniContainer)

	addEnv := &[]corev1.EnvVar{{
		Name: "JAVA_TOOL_OPTIONS",
		Value: "-javaagent:/usr/skywalking/agent/skywalking-agent.jar  " +
			"-Dskywalking.agent.service_name=" + pod.Name +
			"  -Dskywalking.collector.backend_service=oap.skywalking:11800",
	}}

	newEnv := make([]corev1.EnvVar, 0, 0)
	for _, v := range pod.Spec.Containers[0].Env {
		newEnv = append(newEnv, v)
	}
	for _, val := range *addEnv {
		newEnv = append(newEnv, val)
	}

	var containerVolumeMount = &corev1.VolumeMount{
		Name:      "sw-agent",
		MountPath: "/usr/skywalking/agent",
	}

	newContainerVolumeMounts := make([]corev1.VolumeMount, 0, 0)
	for _, v := range pod.Spec.Containers[0].VolumeMounts {
		newContainerVolumeMounts = append(newContainerVolumeMounts, v)
	}
	newContainerVolumeMounts = append(newContainerVolumeMounts, *containerVolumeMount)

	var patches []patchOperation
	patches = append(patches, patchOperation{
		Op:    "replace",
		Path:  "/spec/initContainers",
		Value: newInitContainer,
	}, patchOperation{
		Op:    "replace",
		Path:  "/spec/volumes",
		Value: newVolumes,
	}, patchOperation{
		Op:    "replace",
		Path:  "/spec/containers/0/env",
		Value: newEnv,
	}, patchOperation{
		Op:    "replace",
		Path:  "/spec/containers/0/volumeMounts",
		Value: newContainerVolumeMounts})

	return patches, nil
}

func main() {
	certPath := filepath.Join(tlsDir, tlsCertFile)
	keyPath := filepath.Join(tlsDir, tlsKeyFile)

	mux := http.NewServeMux()
	mux.Handle("/mutate", admitFuncHandler(applyApmDefaults))
	server := &http.Server{
		Addr:    ":8443",
		Handler: mux,
	}
	log.Fatal(server.ListenAndServeTLS(certPath, keyPath))
}
