# skywalking-label

#### 介绍
部署在K8S中的java应用的动态增加skywalking监控。

由于工作中遇到部署在K8S中的java应用的需要开启skywalking的监控，但是又不希望所有的都增加
所以通过k8s的准入控制，设置pod的标签动态开启skywalking-agent
